EXTENSION = pglog
VERS = 2
DATA = pglog--$(VERS).sql
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

all: build

build: 
	rm -f $(DATA)
	cat sql/header.sql > $(DATA)
	for f in sql/functions/*.sql; do cat $$f >> $(DATA); done
	for f in sql/proc/*.sql; do cat $$f >> $(DATA); done
