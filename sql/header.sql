--complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION pglog" to load this file. \quit

create server pglog foreign data wrapper file_fdw;

create schema pglog;

