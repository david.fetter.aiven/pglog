create or replace procedure pglog.create_view_top_n_query(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'top_n_queries') then
      execute format($$drop %s view %I.top_n_queries$$, mat, schema_name);
    end if;
    execute format($$create %s view %I.top_n_queries as (
      with queries as (
      select
        split_part(message, ' ', 2)::decimal as duration,
        regexp_replace
        (
          regexp_replace
          (
            regexp_replace
            (
              regexp_replace
              (
                /* get only the query */
                substring(message from '^(?:duration: \d+.\d+ ms  )?statement: (.*)'),
                /* remove comment */
                '(--[^\n]*)|((\/\*).*(\*\/))',
                ' ',
                'g'
              ),
              /* replace all new lines and unused blanks*/
              '[\r\n]+|\s{2,}|\s$',
              ' ',
              'g'
            ),
            /* Remove string and numeric constant values */
            $text$E?'.*'|(\$.*\$).*\1|\W(-?([0-9]+\.?[0-9]*)|(\.[0-9]+))\W$text$,
            '?',
            'g'
          ),
          '\s\s+',
          ' ',
          'g'
        ) as query
      from
        %I.pglog 
      where
        error_severity = 'LOG'
      and message ~ '^duration'
      and message ~ 'statement'
      and command_tag not in ('BEGIN','END','COMMIT')
    )
    select query,
      count(1),
      avg(duration) as average,
      percentile_disc(0.5) within group (order by duration) as median
    from queries
    group by query
    order by average desc
    )$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;

