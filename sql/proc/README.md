# Procedures documentation
### `create_logtable_from_file(text, text, integer[, text][, boolean])`
Arguments:

- filename text: the name of the file you want to create a table (regular or
    foreign) on top off
- directory text: the directory path where the file is located
- version integer: the Postgres version of the server that generated the logfiles
- schema_name text (optional): the name of the schema you want the program use
    to create the table. Default is `pglog`. The schema needs to exist.
- materialized boolean (optional): if set to `false`, the program will create
    foreign tables, if set to `true`, the programn will create regular tables.
    Default is `false`.

This function will create a table on the file identified by the given filename in
the given directory.
The name of that table will be the filename, stripped off the '.csv' string at the
end and stripped off any special character.

Example:

~~~
pglog=# call pglog.create_logtable_from_file('postgresql-Monday.csv','log','public');
CALL
pglog=# \dE
                 List of relations
 Schema |       Name       |     Type      | Owner 
--------+------------------+---------------+-------
 public | postgresqlmonday | foreign table | elsa
(1 row)
~~~

### `create_logtable_from_dir(text, integer[, text][, boolean]`
Arguments:

- directory text: the directory path where the file is located
- version integer: the Postgres version of the server that generated the logfiles
- schema_name text (optional): the name of the schema you want the program use
    to create the table. Default is `pglog`. The schema needs to exist.
- materialized boolean (optional): if set to `false`, the program will create
    foreign tables, if set to `true`, the programn will create regular tables.
    Default is `false`.

This function will create a table on each csv file found in the directory given as first parameter.

Example:

~~~
pglog=# call pglog.create_logtable_from_file('log');
CALL
pglog=# \dE pglog.*
                 List of relations
 Schema |       Name        |     Type      | Owner 
--------+-------------------+---------------+-------
 pglog  | postgresqlmonday  | foreign table | elsa
 pglog  | postgresqltuesday | foreign table | elsa
(2 rows)
~~~

### `create_mylogs([text][, boolean])`
Arguments:

- schema_name text (optional): the name of the schema you want the program use
    to create the table. Default is `pglog`. The schema needs to exist.
- materialized boolean (optional): if set to `false`, the program will create
    foreign tables, if set to `true`, the programn will create regular tables.
    Default is `false`.

This function will create a fdw on each csv file found in postgres log
directory. Logging parameters should be set as this:

- log_destination: `csvlog`
- logging_collector: `on`

The csv logfiles should have a `.csv` suffix.
The default schema is `pglog`.

Example:

~~~
pglog=# call pglog.create_mylogs();
CALL
pglog=# \dE pglog.*
                 List of relations
 Schema |       Name        |     Type      | Owner 
--------+-------------------+---------------+-------
 pglog  | postgresqlmonday  | foreign table | elsa
 pglog  | postgresqltuesday | foreign table | elsa
(2 rows)
~~~

