create or replace procedure pglog.create_view_tempfile_stats(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    mat text;
  begin
    mat := '';
    if materialized then
      mat := 'materialized';
    end if;
    if pglog.is_object_already_there(schema_name, 'tempfile_report ') then
      execute format($$drop %s view %I.tempfile_report $$, mat, schema_name);
    end if;
    execute format(
      $$create %s view %I.tempfile_report as (
        with temporary_file(size) as (
          /* Doc specifies that should no unit be there, the size is in kB */
          select split_part(message, ', size ', 2)::bigint*1024
          from %I.pglog
          where error_severity = 'LOG'
            and message ~ '^temporary file:.*, size '
        )
        select
          pg_size_pretty(min(size)) as min,
          pg_size_pretty(max(size)) as max,
          pg_size_pretty(avg(size)) as global_average,
          pg_size_pretty(percentile_disc(0.1) within group (order by size)) as "10%%",
          pg_size_pretty(percentile_disc(0.2) within group (order by size)) as "20%%",
          pg_size_pretty(percentile_disc(0.3) within group (order by size)) as "30%%",
          pg_size_pretty(percentile_disc(0.4) within group (order by size)) as "40%%",
          pg_size_pretty(percentile_disc(0.5) within group (order by size)) as "50%%",
          pg_size_pretty(percentile_disc(0.6) within group (order by size)) as "60%%",
          pg_size_pretty(percentile_disc(0.7) within group (order by size)) as "70%%",
          pg_size_pretty(percentile_disc(0.8) within group (order by size)) as "80%%",
          pg_size_pretty(percentile_disc(0.9) within group (order by size)) as "90%%",
          pg_size_pretty(percentile_disc(1) within group (order by size)  ) as "100%%"
        from
          temporary_file)$$, mat, schema_name, schema_name);
  end
$body$
language plpgsql;
