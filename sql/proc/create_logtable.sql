/* this procedure will create the logtable in the given schema */
create or replace procedure pglog.create_logtable(schema_name text,
  logversion integer) as
$body$
  begin
    if logversion < 13 then
      /* The following lines will be removed when Postgres 12 won't be
       * supported anymore */
      execute format(
        $$create table if not exists %I.pglog (
          log_time timestamp(3) with time zone,
          user_name text,
          database_name text,
          process_id integer,
          connection_from text,
          session_id text,
          session_line_num bigint,
          command_tag text,
          session_start_time timestamp with time zone,
          virtual_transaction_id text,
          transaction_id bigint,
          error_severity text,
          sql_state_code text,
          message text,
          detail text,
          hint text,
          internal_query text,
          internal_query_pos integer,
          context text,
          query text,
          query_pos integer,
          location text,
          application_name text
        ) partition by range (log_time)$$, schema_name);
    elsif logversion < 14 then
      /* Postgres 13 added the backend_type column */
      execute format(
        $$create table if not exists %I.pglog (
          log_time timestamp(3) with time zone,
          user_name text,
          database_name text,
          process_id integer,
          connection_from text,
          session_id text,
          session_line_num bigint,
          command_tag text,
          session_start_time timestamp with time zone,
          virtual_transaction_id text,
          transaction_id bigint,
          error_severity text,
          sql_state_code text,
          message text,
          detail text,
          hint text,
          internal_query text,
          internal_query_pos integer,
          context text,
          query text,
          query_pos integer,
          location text,
          application_name text,
          backend_type text
        ) partition by range (log_time)$$, schema_name);
    else
       /* Postgres 14 has inherited the backend_type column from Postgres 13
        * and added the leader_pid and the query_id columns.*/
      execute format(
        $$create table if not exists %I.pglog (
          log_time timestamp(3) with time zone,
          user_name text,
          database_name text,
          process_id integer,
          connection_from text,
          session_id text,
          session_line_num bigint,
          command_tag text,
          session_start_time timestamp with time zone,
          virtual_transaction_id text,
          transaction_id bigint,
          error_severity text,
          sql_state_code text,
          message text,
          detail text,
          hint text,
          internal_query text,
          internal_query_pos integer,
          context text,
          query text,
          query_pos integer,
          location text,
          application_name text,
          backend_type text,
          leader_pid integer,
          query_id bigint
        ) partition by range (log_time)$$, schema_name);
    end if;
  end;
$body$
language 'plpgsql';

