create or replace procedure pglog.create_all_views(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  begin
    execute format($$call pglog.create_view_top_n_query(%L, %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_error_report(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_tempfile_stats(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_tempfile_queries(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_autovacuum_report(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_checkpoints_stats(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_view_checkpoints_stats_by_hour(%L,  %L::boolean)$$,
      schema_name, materialized);
    execute format($$call pglog.create_views_error_analyze(%L,  %L::boolean)$$,
      schema_name, materialized);
  end
$body$
language plpgsql;

