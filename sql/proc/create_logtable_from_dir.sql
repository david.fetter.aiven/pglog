/* This procedure will create several logtables for all csv files inside the
 * given directory.
 * If the materialized boolean is false, it will create FDWs
 * If the materialized boolean is true, it will create real tables */
create or replace procedure pglog.create_logtable_from_dir(
  directory text,
  logversion integer,
  schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    my_files record;
    my_function text;
    is_new boolean := false;
  begin
    if not pglog.is_object_already_there(schema_name, 'pglog')
    then
      execute format($$call pglog.create_logtable(%L,%L::integer)$$, schema_name, logversion);
        is_new := true;
    end if;
    if materialized
    then
      for my_files in
        execute format($$select filename from pg_ls_dir(%L) as filename where filename ~ '.csv'$$, directory)
      loop
        execute format($$call pglog.create_real_table_from_file(%L, %L, %L::integer, %L)$$, my_files.filename, directory, logversion, schema_name);
      end loop;
      if is_new
      then
        execute format($$analyze %I.pglog$$, schema_name);
        execute format($$create index on %I.pglog(error_severity)$$, schema_name);
      end if;
    else
      for my_files in
        execute format($$select filename from pg_ls_dir(%L) as filename where filename ~ '.csv'$$, directory)
      loop
        execute format($$call pglog.create_foreign_table_from_file(%L, %L, %L::integer, %L)$$, my_files.filename, directory, logversion, schema_name);
      end loop;
    end if;
    execute format($$call pglog.create_all_views(%L, %L::boolean)$$, schema_name, materialized);
  end;
$body$
language plpgsql;

