/* This pricedure will create log tables for all the logfiles in csv format
 * found in the log_destination dirrectory.*/
create or replace procedure pglog.create_mylogs(schema_name text default 'pglog',
  materialized boolean default false) as
$body$
  declare
    directory text;
    log_destination text;
    logging_collector boolean;
    logversion integer;
  begin
    execute $$select setting::integer/10000 from pg_settings where name = 'server_version_num'$$ into logversion;
    execute $$select setting from pg_settings where name = 'log_destination'$$ into log_destination;
    if log_destination <> 'csvlog' then
      raise exception $$The only acceptable value for the 'log_destination' parameter is 'csvlog'$$;
    end if;
    execute $$select setting from pg_settings where name = 'logging_collector'$$ into logging_collector;
    if not logging_collector then
      raise exception $$The only acceptable value for the 'logging_collector' parameter is 'true'$$;
    end if;
    execute $$select setting from pg_settings where name = 'log_directory'$$ into directory;
    execute format($$call pglog.create_logtable_from_dir(%L,%L::integer,%L,%L::boolean)$$, directory, logversion, schema_name, materialized);
  end;
$body$
language plpgsql;

